/*
 *    OzaBag.java
 *    Copyright (C) 2007 University of Waikato, Hamilton, New Zealand
 *    @author Richard Kirkby (rkirkby@cs.waikato.ac.nz)
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program. If not, see <http://www.gnu.org/licenses/>.
 *    
 */
package moa.classifiers.meta;

import com.github.javacliparser.IntOption;
import com.yahoo.labs.samoa.instances.Instance;
import moa.classifiers.AbstractClassifier;
import moa.classifiers.Classifier;
import moa.core.DoubleVector;
import moa.core.Measurement;
import moa.core.MiscUtils;
import moa.options.ClassOption;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
* Based on the implementation of OzaBag
 */
public class AES extends AbstractClassifier {

    @Override
    public String getPurposeString() {
        return "Incremental on-line bagging of Oza and Russell.";
    }
        
    private static final long serialVersionUID = 1L;

    public ClassOption baseLearnerOption = new ClassOption("baseLearner", 'l',
            "Classifier to train.", Classifier.class, "trees.HoeffdingTree");
    public IntOption ensembleSizeOption = new IntOption("ensembleSize", 's',
            "The number of models in the bag.", 10, 1, Integer.MAX_VALUE);
    private int ensembleSize = 10;
    Measurement[] measure;
    Double selializedMem0;
    Double memIncrease0;
    Double memIncrease1;
    LinkedList<Double> serializedMem = new LinkedList();


    protected List<Classifier> ensemble;

    @Override
    public void resetLearningImpl() {
        this.ensemble = new ArrayList<Classifier>();
        Classifier baseLearner = (Classifier) getPreparedClassOption(this.baseLearnerOption);
        baseLearner.resetLearning();

        for (int i = 0; i < ensembleSize; i++) {
            this.ensemble.add(baseLearner.copy());
        }
        for (int i = 0; i < 3; i++){
            this.serializedMem.add(null);
        }
    }

    @Override
    public void trainOnInstanceImpl(Instance inst) {
        if (memIncrease0 != null && memIncrease1 != null){
            if (memIncrease1 > memIncrease0 && ensembleSize < 25)
                ensembleSize += 1;
            if (memIncrease1 < memIncrease0 && ensembleSize > 10)
                ensembleSize -= 1;
        }
        if (ensembleSize > ensemble.size()){
            Classifier baseLearner = (Classifier) getPreparedClassOption(baseLearnerOption);
            baseLearner.resetLearning();
            for (int j = ensemble.size(); j < ensembleSize; j++)
                ensemble.add(baseLearner.copy());
        }
        for (int i = 0; i < ensembleSize; i++) {
            int k = MiscUtils.poisson(1.0, this.classifierRandom);
            if (k > 0) {
                Instance weightedInst = (Instance) inst.copy();
                weightedInst.setWeight(inst.weight() * k);
                ensemble.get(i).trainOnInstance(weightedInst);
            }
        }
        if (Measurement.measurementMap != null) {
            selializedMem0 = Measurement.measurementMap.get("[avg]model serialized size (bytes)");
        }

        serializedMem.add(selializedMem0);
        if (serializedMem.size() > 3)
            serializedMem.remove();

        if (serializedMem.get(0) != null && serializedMem.get(1) != null){
            if (serializedMem.get(0).doubleValue() != serializedMem.get(1).doubleValue())
                memIncrease0 = Math.abs(serializedMem.get(0) - serializedMem.get(1));
        }
        if (serializedMem.get(1) != null && serializedMem.get(2) != null && memIncrease0 != null){
            if (serializedMem.get(1).doubleValue() != serializedMem.get(2).doubleValue()){
                Double t = Math.abs(serializedMem.get(1) - serializedMem.get(2));
                if (memIncrease0.doubleValue() != t.doubleValue())
                    memIncrease1 = t;
            }
        }
    }

    @Override
    public double[] getVotesForInstance(Instance inst) {
        DoubleVector combinedVote = new DoubleVector();
        for (int i = 0; i < ensembleSize; i++) {
            DoubleVector vote = new DoubleVector(ensemble.get(i).getVotesForInstance(inst));
            if (vote.sumOfValues() > 0.0) {
                vote.normalize();
                combinedVote.addValues(vote);
            }
        }
        return combinedVote.getArrayRef();
    }

    @Override
    public boolean isRandomizable() {
        return true;
    }

    @Override
    public void getModelDescription(StringBuilder out, int indent) {
        // TODO Auto-generated method stub
    }

    @Override
    protected Measurement[] getModelMeasurementsImpl() {
        return new Measurement[]{new Measurement("ensemble size",
                ensemble.isEmpty() ? 0 : ensembleSize)};
    }

    @Override
    public Classifier[] getSubClassifiers() {
        return ensemble.toArray(new Classifier[ensemble.size()]).clone();
    }
}
