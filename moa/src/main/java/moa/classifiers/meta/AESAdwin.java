/*
 *    OzaBagAdwin.java
 *    Copyright (C) 2008 University of Waikato, Hamilton, New Zealand
 *    @author Albert Bifet (abifet at cs dot waikato dot ac dot nz)
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program. If not, see <http://www.gnu.org/licenses/>.
 *    
 */
package moa.classifiers.meta;

import com.github.javacliparser.IntOption;
import com.yahoo.labs.samoa.instances.Instance;
import moa.classifiers.AbstractClassifier;
import moa.classifiers.Classifier;
import moa.classifiers.core.driftdetection.ADWIN;
import moa.core.DoubleVector;
import moa.core.Measurement;
import moa.core.MiscUtils;
import moa.options.ClassOption;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Based on the implementation of OzaBagAdwin
 */
public class AESAdwin extends AbstractClassifier {

    private static final long serialVersionUID = 1L;

    @Override
    public String getPurposeString() {
        return "Bagging for evolving data streams using ADWIN.";
    }

    public ClassOption baseLearnerOption = new ClassOption("baseLearner", 'l',
            "Classifier to train.", Classifier.class, "trees.HoeffdingTree");

    private int ensembleSize = 10;
    Measurement[] measure;
    Double serializedMem0;
    LinkedList<Double> serializedMem = new LinkedList<Double>();
    Double memIncrease0;
    Double memIncrease1;

    public IntOption ensembleSizeOption = new IntOption("ensembleSize", 's',
            "The number of models in the bag.", 10, 1, Integer.MAX_VALUE);

    protected List<Classifier> ensemble;

    protected ADWIN[] ADError;

    @Override
    public void resetLearningImpl() {
        this.ensemble = new ArrayList<Classifier>();

        Classifier baseLearner = (Classifier) getPreparedClassOption(this.baseLearnerOption);
        baseLearner.resetLearning();

        for (int i = 0; i < ensembleSize; i++) {
            this.ensemble.add(baseLearner.copy());
        }

        this.ADError = new ADWIN[this.ensembleSize];

        for (int i = 0; i < this.ensembleSize; i++) {
            this.ADError[i] = new ADWIN();
        }

        for (int i = 0; i < 3; i++)
            serializedMem.add(null);
    }

    @Override
    public void trainOnInstanceImpl(Instance inst) {

        if (memIncrease0 != null && memIncrease1 != null){
            if (memIncrease1 > memIncrease0 && ensembleSize < 25)
                ensembleSize += 1;
            if (memIncrease1 < memIncrease0 && ensembleSize > 10)
                ensembleSize -= 1;
        }

        this.ADError = new ADWIN[this.ensembleSize];
        for (int i = 0; i < this.ensembleSize; i++) {
            this.ADError[i] = new ADWIN();
        }

        if (ensembleSize > ensemble.size()){
            Classifier baseLearner = (Classifier) getPreparedClassOption(this.baseLearnerOption);
            baseLearner.resetLearning();
            for (int j = ensemble.size(); j < ensembleSize; j++)
                this.ensemble.add(baseLearner.copy());
        }

        boolean Change = false;


        for (int i = 0; i < this.ensembleSize; i++) {
            int k = MiscUtils.poisson(1.0, this.classifierRandom);
            if (k > 0) {
                Instance weightedInst = (Instance) inst.copy();
                weightedInst.setWeight(inst.weight() * k);
                this.ensemble.get(i).trainOnInstance(weightedInst);
            }

            boolean correctlyClassifies = this.ensemble.get(i).correctlyClassifies(inst);
            double ErrEstim = this.ADError[i].getEstimation();
            if (this.ADError[i].setInput(correctlyClassifies ? 0 : 1)) {
                if (this.ADError[i].getEstimation() > ErrEstim) {
                    Change = true;
                }
            }
        }


        if (Change) {
            double max = 0.0;
            int imax = -1;
            for (int i = 0; i < this.ensembleSize; i++) {
                if (max < this.ADError[i].getEstimation()) {
                    max = this.ADError[i].getEstimation();
                    imax = i;
                }
            }

            if (imax != -1) {
                this.ensemble.get(imax).resetLearning();
                //this.ensemble[imax].trainOnInstance(inst);
                this.ADError[imax] = new ADWIN();
            }
        }

        if (Measurement.measurementMap != null)
            serializedMem0 = Measurement.measurementMap.get("[avg]model serialized size (bytes)");

        serializedMem.add(serializedMem0);
        if (serializedMem.size() > 3)
            serializedMem.remove();


        if (serializedMem.get(0) != null && serializedMem.get(1) != null){
            if (serializedMem.get(0).doubleValue() != serializedMem.get(1).doubleValue())
                memIncrease0 = Math.abs(serializedMem.get(0) - serializedMem.get(1));
        }

        if (serializedMem.get(1) != null && serializedMem.get(2) != null && memIncrease0 != null){
            if (serializedMem.get(1).doubleValue() != serializedMem.get(2).doubleValue()){
                Double t = Math.abs(serializedMem.get(1) - serializedMem.get(2));
                if (memIncrease0.doubleValue() != t.doubleValue())
                    memIncrease1 = t;
            }
        }

    }

    @Override
    public double[] getVotesForInstance(Instance inst) {
        DoubleVector combinedVote = new DoubleVector();
        for (int i = 0; i < this.ensembleSize; i++) {
            DoubleVector vote = new DoubleVector(this.ensemble.get(i).getVotesForInstance(inst));
            if (vote.sumOfValues() > 0.0) {
                vote.normalize();
                combinedVote.addValues(vote);
            }
        }
        return combinedVote.getArrayRef();
    }

    @Override
    public boolean isRandomizable() {
        return true;
    }

    @Override
    public void getModelDescription(StringBuilder out, int indent) {
        // TODO Auto-generated method stub
    }

    @Override
    protected Measurement[] getModelMeasurementsImpl() {
        return new Measurement[]{new Measurement("ensemble size",
                this.ensemble.isEmpty() ? 0 : ensembleSize)};
    }

    @Override
    public Classifier[] getSubClassifiers() {
        return this.ensemble.toArray(new Classifier[ensemble.size()]).clone();
    }
}